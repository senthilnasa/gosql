package gosql

import (
	"encoding/hex"
	"hash/fnv"
	"strconv"
	"strings"
	"time"
)

func GenerateHash() uint32 {
	hash := fnv.New32a()
	hash.Write([]byte(strconv.FormatInt(time.Now().UnixNano(), 10)))
	return hash.Sum32()
}

func needToEscape(str string) bool {
	return strings.Contains(str, ";") ||
		strings.Contains(str, "\"") ||
		strings.Contains(str, "'") ||
		strings.Contains(str, ";") ||
		strings.Contains(str, "\\") ||
		strings.Contains(str, "\n") ||
		strings.Contains(str, "\r")
}

func bytesToString(bytes []byte) string {
	return "x'" + hex.EncodeToString(bytes) + "'"
}

func PrepareStringValue(val interface{}) string {
	var strVal string
	switch val.(type) {
	case string:
		strVal = val.(string)
		if needToEscape(strVal) {
			strVal = bytesToString([]byte(strVal))
		} else {
			strVal = "'" + strVal + "'"
		}

	case []byte:
		strVal = bytesToString(val.([]byte))

	case int:
		strVal = strconv.FormatInt(int64(val.(int)), 10)

	case int64:
		strVal = strconv.FormatInt(val.(int64), 10)

	default:
		panic("Unexpected type of value. Supported types: string, []byte, int, int64")
	}
	return strVal
}

func ConcatValues(begin string, values []string, delim string, end string) string {
	return begin + strings.Join(values, delim) + end
}

// datetime to unix
func _(datetime string) int64 {

	if strings.HasPrefix(datetime, "0000") {
		return 0
	}

	loc, _ := time.LoadLocation("Europe/Kiev")
	date, err := time.ParseInLocation("2006-01-02 15:04:05", datetime, loc)

	if err != nil {
		panic(err)
	}

	return date.Unix()
}