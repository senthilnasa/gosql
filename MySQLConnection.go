package gosql

import (
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/xlab/closer"
	"strconv"
	"sync"
	"time"
)

//////////////////// MySQLConfig ////////////////////////
type MySQLConfig struct {
	Host   string
	Port   int
	User   string
	Pass   string
	DbName string
}

func NewMySQLConfig(host string, port int, user string, password string, dataBase string) *MySQLConfig {
	return &MySQLConfig{host, port, user, password, dataBase}
}


func (conf *MySQLConfig) String() string {
	return conf.User + "@" + conf.Host + ":" + strconv.Itoa(conf.Port) + "/" + conf.DbName
}


/////////////////// MySQLConnection //////////////////////
type MySQLConnection struct {
	hash uint32
	Config   *MySQLConfig
	dataBase *sql.DB
	isTransaction bool
	transaction *sql.Tx
	admin *MySQLAdmin
}

// Get connection without error.
// If can't connect - panic
func GetMySQLConnection(host string, port int, user string, password string, dataBase string) MySQLConnection {
	conf := NewMySQLConfig(host, port, user, password, dataBase)
	conn, err := CreateMySQLConnection(conf)
	if err != nil {
		panic(err)
	}
	return conn
}

// Get connection with error
func NewMySQLConnection(host string, port int, user string, password string, dataBase string) (MySQLConnection, error) {
	conf := NewMySQLConfig(host, port, user, password, dataBase)
	return CreateMySQLConnection(conf)
}

// Create connection to database. Working with Bind.close for auto closing connection
func CreateMySQLConnection(config *MySQLConfig) (MySQLConnection, error) {

	db := MySQLConnection{Config: config, hash:GenerateHash(), isTransaction:false}
	err := db.connect()
	if err != nil {
		fmt.Println(err)
		return db, err
	}

	closer.Bind(db.Close)
	return db, nil
}

func (db *MySQLConnection) checkConnection() {
	err := db.dataBase.Ping()
	if err != nil {
		fmt.Println("DB reconnect")
		_ = db.connect()
	}
}

func (db *MySQLConnection) connect() error {
	dataBase, err := sql.Open("mysql", fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		db.Config.User, db.Config.Pass, db.Config.Host, db.Config.Port, db.Config.DbName))
	if err != nil {
		return err
	}

	err = dataBase.Ping()
	if err != nil {
		return err
	}

	dataBase.SetConnMaxLifetime(10 * time.Minute)
	db.dataBase = dataBase
	return nil
}

func (db *MySQLConnection) Hash() uint32 {
	return db.hash
}

func (db *MySQLConnection) Close() {
	_ = db.dataBase.Close()
}

func _()  {
	fmt.Println(mysql.ErrBusyBuffer)
}

func (db *MySQLConnection) String() string {
	return "[id=" + fmt.Sprint(db.hash) + "] [" + db.Config.String() + "]"
}


///////////////// MySQLConnectionPool ////////////////////
type MySQLConnectionPool struct {
	sync.Mutex
	blocked map[uint32]bool
	connections []MySQLConnection
}

func NewMySQLConnectionPool(size int, config *MySQLConfig) (*MySQLConnectionPool, error) {

	pool := &MySQLConnectionPool{connections:make([]MySQLConnection, size), blocked:make(map[uint32]bool)}
	for i := 0; i < size; i++ {
		connection, err := CreateMySQLConnection(config)
		if err == nil {
			pool.connections[i] = connection
			pool.blocked[connection.hash] = false
		} else {
			return nil, err
		}
	}

	fmt.Println("Pool created")
	return pool, nil
}

func (pool *MySQLConnectionPool) Get() *MySQLConnection {

	pool.Lock()
	defer pool.Unlock()

	for _, con := range pool.connections {
		if !pool.blocked[con.hash] {
			pool.blocked[con.hash] = true
			con.checkConnection()
			return &con
		}
	}

	time.Sleep(500 * time.Millisecond)
	return pool.Get()
}

func (pool *MySQLConnectionPool) Release(connection *MySQLConnection) {

	pool.Lock()
	defer pool.Unlock()

	pool.blocked[connection.hash] = false
}

func (pool *MySQLConnectionPool) Size() int {

	pool.Lock()
	defer pool.Unlock()

	size := 0
	for _, blocked := range pool.blocked {
		if !blocked {
			size++
		}
	}
	return size
}

func (pool *MySQLConnectionPool) CloseAll() {
	for _, c := range pool.connections {
		pool.blocked[c.hash] = true
		c.Close()
	}
	fmt.Println("Connections are closed")
}

func (pool *MySQLConnectionPool) String() string {
	poolInfo := "Pool: \n"
	for _, c := range pool.connections {
		conInfo := "[blocked=" + strconv.FormatBool(pool.blocked[c.hash]) + "] " + c.String() + "\n"
		poolInfo += conInfo
	}
	return poolInfo
}


///////////////// Replication ////////////////////
func (db *MySQLConnection) ReplicationStatus() *ReplicationInfo {
	rs, err := db.Select("SHOW SLAVE STATUS")
	if err == nil {
		if rs.Next() {

			slaveLagStr := rs.GetString("Seconds_Behind_Master")
			slaveIORunning := rs.GetString("Slave_IO_Running")
			slaveSQLRunning := rs.GetString("Slave_SQL_Running")
			slaveLag, err2 := strconv.Atoi(slaveLagStr)
			status := slaveIORunning == "Yes" && slaveSQLRunning == "Yes" && err2 == nil

			return &ReplicationInfo {
				Status: status,
				SlaveLag: slaveLag,
				MasterHost: rs.GetString("Master_Host"),
				BinlogFile: rs.GetString("Relay_Master_Log_File"),
				BinlogPos: rs.GetString("Exec_Master_Log_Pos"),
			}
		} else {
			return &ReplicationInfo{Status:false}
		}
	} else {
		panic(err)
	}
}

func (db *MySQLConnection) ReplicationStop() {
	_, err := db.ExecuteInsert("STOP SLAVE;")
	if err != nil {
		panic(err)
	}
}

func (db *MySQLConnection) ReplicationReset() {
	_, err := db.ExecuteInsert("RESET SLAVE;")
	if err != nil {
		panic(err)
	}
}

func (db *MySQLConnection) ReplicationStart() {
	_, err := db.ExecuteInsert("START SLAVE;")
	if err != nil {
		panic(err)
	}
}

func (db *MySQLConnection) ReplicationSetup(host, user, pass, binlog, pos string) {
	change := "CHANGE MASTER TO MASTER_HOST='" + host + "', MASTER_USER='" + user + "', MASTER_PASSWORD='" + pass + "', MASTER_LOG_FILE='" + binlog + "', MASTER_LOG_POS=" + pos + ";"
	_, err := db.ExecuteInsert(change)
	if err != nil {
		panic(err)
	}
}


////////////////////// VARIABLES /////////////////////////////
func (db *MySQLConnection) ShowConfigVariablesLike(pattern string) *MySQLVariables {
	show := "SHOW VARIABLES LIKE '" + pattern + "';"
	res, err := db.Select(show)
	if err == nil {
		result := &MySQLVariables{vars: make(map[string]string)}
		for res.Next() {
			result.vars[res.GetString("Variable_name")] = res.GetString("Value")
		}
		return result
	} else {
		panic(err)
	}
}