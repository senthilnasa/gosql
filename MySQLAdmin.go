package gosql

import (
	"errors"
	"fmt"
	"strings"
)

type MySQLAdmin struct {
	mysqlDb MySQLConnection
}

func (db *MySQLConnection) Admin() *MySQLAdmin {
	if db.admin == nil {
		dbConfig := db.Config
		dbConfig.DbName = "mysql"

		mysqlDBConnection, err := CreateMySQLConnection(dbConfig)
		if err != nil {
			panic(err)
		}
		db.admin = &MySQLAdmin{mysqlDb:mysqlDBConnection}
	}
	return db.admin
}

////////////////////// STRUCTURE //////////////////////////////
func (dba *MySQLAdmin) CreateDatabase(dbName, charset, collation string) error {
	if len(charset) == 0 {
		charset = "utf8"
	}
	if len(collation) == 0 {
		collation = "utf8_general_ci"
	}
	_, err := dba.mysqlDb.ExecuteInsert(fmt.Sprintf("CREATE DATABASE `%s` CHARACTER SET %s COLLATE %s;", dbName, charset, collation))
	if err != nil {
		return err.Error()
	}
	return nil
}

func (dba *MySQLAdmin) DropDatabase(dbName string) error {
	_, err := dba.mysqlDb.ExecuteInsert(fmt.Sprintf("DROP DATABASE `%s`", dbName))
	if err != nil {
		return err.Error()
	}
	return nil
}

func (dba *MySQLAdmin) CheckDatabaseExists(dbName string) (bool, error) {
	res, err := dba.mysqlDb.Select("SELECT `SCHEMA_NAME` FROM `information_schema`.`schemata` WHERE `SCHEMA_NAME` = ?;", dbName)
	if err != nil {
		return false, err.Error()
	}
	return res.Next(), nil
}

func (dba *MySQLAdmin) RenameDatabaseWithDebug(oldName, newName string) error {
	return dba.renameDatabase(oldName, newName, true)
}

func (dba *MySQLAdmin) RenameDatabase(oldName, newName string) error {
	return dba.renameDatabase(oldName, newName, false)
}

func (dba *MySQLAdmin) renameDatabase(oldName, newName string, debug bool) error {

	oldDbExists, err := dba.CheckDatabaseExists(oldName)
	if err != nil {
		return err
	}
	if !oldDbExists {
		return errors.New("Database with name " + oldName + " does not exists")
	}

	newDbExists, err := dba.CheckDatabaseExists(newName)
	if err != nil {
		return err
	}
	if newDbExists {
		return errors.New("Database with name " + newName + " already exists")
	}

	err2 := dba.CreateDatabase(newName, "", "")
	if err2 != nil {
		return err2
	}

	str, err := dba.GetMySQLDBStructure(oldName, true, false)
	if err != nil {
		return err
	}

	dbConfig := dba.mysqlDb.Config
	dbConfig.DbName = newName
	newDbConnection, err := CreateMySQLConnection(dbConfig)
	if err != nil {
		return err
	}

	for tableName := range str.Tables {
		renameSQL := fmt.Sprintf("RENAME TABLE `%s`.`%s` TO `%s`.`%s`;", oldName, tableName, newName, tableName)
		_, err2 := newDbConnection.ExecuteInsert(renameSQL)
		if err2 != nil {
			return err2.Error()
		}
		if debug {
			fmt.Println(tableName)
		}
	}

	for procName, procCreateSQL := range str.Routines {
		_, err := newDbConnection.ExecuteInsert(procCreateSQL)
		if err != nil {
			return err.Error()
		}
		if debug {
			fmt.Println(procName)
		}
	}

	err3 := dba.DropDatabase(oldName)
	if err3 != nil {
		return err3
	}
	return nil
}

func (dba *MySQLAdmin) GetMySQLDBStructure(dbname string, withRoutines, withEvents bool) (*MySQLDBStructure, error) {

	if withEvents {
		return nil, errors.New("events dumping unsupported. Check in future releases")
	}

	resTbls, err := dba.mysqlDb.Select("SELECT `TABLE_NAME` FROM `information_schema`.`tables` WHERE `TABLE_SCHEMA` = ?", dbname)
	if err != nil {
		return nil, err.Error()
	}

	tables := make(map[string]string)
	for resTbls.Next() {
		tableName := resTbls.GetString(1)
		resTbl, err := dba.mysqlDb.Select(fmt.Sprintf("SHOW CREATE TABLE `%s`.`%s`;", dbname, tableName))
		if err != nil {
			return nil, err.Error()
		}
		if resTbl.Next() {
			createTableSQL := resTbl.GetString(2)
			if !strings.HasPrefix(createTableSQL, "CREATE TABLE ") {
				panic("Strange create table statement: " + createTableSQL)
			}
			tables[tableName] = createTableSQL
		}
	}

	routines := make(map[string]string)
	if withRoutines {
		resRtns, err := dba.mysqlDb.Select("SELECT `ROUTINE_NAME`, `ROUTINE_TYPE` FROM `information_schema`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = ?;", dbname)
		if err != nil {
			return nil, err.Error()
		}

		for resRtns.Next() {
			routineName := resRtns.GetString(1)
			routineType := resRtns.GetString(2)

			resRtn, err := dba.mysqlDb.Select(fmt.Sprintf("SHOW CREATE %s `%s`.`%s`;", routineType, dbname, routineName))
			if err != nil {
				return nil, err.Error()
			}
			if resRtn.Next() {
				createRoutineSQL := resRtn.GetString(3)
				if !strings.HasPrefix(createRoutineSQL, "CREATE ") {
					panic("Strange create routine statement: " + createRoutineSQL)
				}
				routines[routineName] = createRoutineSQL
			}
		}
	}

	return &MySQLDBStructure{Tables:tables, Routines:routines}, nil
}